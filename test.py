#!/usr/bin/python
# -*- coding:utf-8 -*-
import os,sys
import jieba
#from jieba import posseg, analyse
from elasticsearch import Elasticsearch
es = Elasticsearch()
x = es.search(index='music',doc_type='The Downtown Fiction',body={"query":{"bool":{"must":[],"must_not":[],"should":[{"match_all":{}}]}},"from":0,"size":250,"sort":[],"aggs":{}})['hits']['hits']
file = open(os.path.split(os.path.realpath(__file__))[0] + '\\util\\user_dict.txt','w')
for i in x:
    file.write((i['_source']['sing'] + u' 1 n\n').encode('utf-8'))
file.close()
jieba.load_userdict(os.path.split(os.path.realpath(__file__))[0] + '\\util\\user_dict.txt')
l = ['点一首邓紫棋的歌',
     '放一首雅俗共赏',
     '来一首G.E.M的千年一般若',
     '放一曲龙梅子的怒放']
for i in l:
    a = jieba.cut(i)
    print ','.join(a)
    b = jieba.cut(i,cut_all=True)
    print ','.join(b)
    c = jieba.cut_for_search(i)
    print ','.join(c)
    pass