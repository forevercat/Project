#!/usr/bin/python
#encoding:utf-8
import os
import jieba
class JiebaCut():
    def __init__(self):
        jieba.load_userdict(os.path.split(os.path.realpath(__file__))[0] + '\\user_dict.txt')
        jieba

    def cut(self,txt):
        stop = [line.strip().decode('utf-8') for line in open('stop.txt').readlines() ]
        segs = jieba.cut(txt)
        text = ','.join(list(set(segs)-set(stop)))
        return text
        return jieba.cut(txt)
