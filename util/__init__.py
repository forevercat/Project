import random
import os
import types

def gen_uuid():
    return ''.join(random.sample('ABCDEFGHJKLMNPQRSTUVWXYZ0123456789', 16)) 

def gen_random_file(suffix=''):
    filename = ''.join(random.sample('ABCDEFGHJKLMNPQRSTUVWXYZ0123456789', 16))
    if suffix:
        filename = '.'.join([filename, suffix])
    return os.path.join('/dev/shm', filename)

def json_to_str(obj):
    tp = type(obj)
    if tp == types.DictType:
        obj = dict((k, json_to_str(obj[k])) for k in obj)
    elif tp == types.ListType:
        obj = [json_to_str(item) for item in obj]
    elif tp == types.NoneType:
        obj = None
    else:
        obj = str(obj)
    return obj

