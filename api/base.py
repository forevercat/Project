#!/usr/bin/python
#encoding=utf-8
import sys
import json

import tornado.web
from tornado import httputil

import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import func
from elasticsearch import Elasticsearch
import api
import util.config

from util import json_to_str

reload(sys)
sys.setdefaultencoding('utf-8')

class BaseHandler(tornado.web.RequestHandler):
    __model__ = '__base__'

    def __init__(self, application, request, **kwargs):
        super(BaseHandler, self).__init__(application, request, **kwargs)
        self.es = Elasticsearch()
        Session = sessionmaker(bind=application.engine)
        self.session = Session()

    def get(self, op = None):
        if not hasattr(self, op):
            self.write("unsupport method")
            return
        getattr(self, op)()

    def post(self, op = None):
        if not hasattr(self, op):
            self.write("unsupport method")
            return
        getattr(self, op)()


    def response(self, result = {'status':'ok'}, code = 200, reason = None):
        self.set_status(code, reason)
        if not result:
            self.write("")
            self.finish()
            return

        self.set_header("Content-Type", "application/json")
        self.set_header("Cache-Control", "no-cache")
        if result.get('code') == None:
            result['code'] = 0
        if result.get('errmsg') == None:
            result['errmsg'] = 'Ok'
        self.write(json_to_str(result))
        self.finish()

    def on_finish(self):
        self.session.close()

    def extract_arguments(self, required):
        arguments = {}
        for arg in required:
            v = self.get_argument(arg, "")
            if not v:
                resp = {'code': 400, 'errmsg': 'missing argument %s' % arg}
                self.response(resp, 400)
                return None
            arguments[arg] = v
        return arguments

    def get_client_ip(self):
        return self.request.headers['X-Real-Ip'] or self.request.remote_ip
