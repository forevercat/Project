#!/usr/bin/python
# -*- coding=utf-8 -*-
import base

class ProgramHandler(base.BaseHandler):

    def index(self):
        args = self.extract_arguments(['key'])
        key_list = args['key'].split(',')
        query = ''
        for k in key_list:
            query += k + ' '
        body ={
                "query": {
                "bool": {
                "must": [
                {
                "query_string": {
                "default_field": "_all",
                "query": query
                }
                }
                ],
                "must_not": [ ],
                "should": [ ]
                }
                },
                "from": 0,
                "size": 10000,
                "sort": [ ],
                "aggs": { }
                }
        result = self.es.search(
            index='act',
            body=body)['hits']['hits']
        self.response({'data':result})
