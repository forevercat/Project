#!/usr/bin/python
# -*- coding=utf-8 -*-
import base

class UserHandler(base.BaseHandler):

    def login(self):
        args = self.extract_arguments(['login_id','password'])
        self.response({'login_id':args['login_id'],'password':args['password']})
