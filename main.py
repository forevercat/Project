#!/usr/bin/python
import sys
import api
import types
import tornado.ioloop
import tornado.web
import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import func
import util.config

reload(sys)
sys.setdefaultencoding('utf-8')

class ESApplication(tornado.web.Application):
    def __init__(self, api_entry, **settings):
        handlers = self.load_handlers(api_entry)

        username = util.config.get("mysql", "username")
        password = util.config.get("mysql", "password")
        host = util.config.get("mysql", "host")
        database = util.config.get("mysql", "database")
        
        self.engine = create_engine('mysql://%s:%s@%s/%s?charset=utf8' % (username, password, host, database),\
              encoding='utf-8', echo=False, pool_size=20, max_overflow=0)
    
        super(ESApplication, self).__init__(handlers, **settings)

    def load_handlers(self, m):
        handlers = []
        if hasattr(m, "__all__"):
            for sub_module in m.__all__:
                __import__("%s.%s" % (m.__name__, sub_module))
        for attr in dir(m):
            if attr.startswith("__"):
                continue
            inst = getattr(m, attr)
            if type(inst) == types.ModuleType:
                if not inst.__name__.startswith(m.__name__):
                    continue
                else:
                    handlers += self.load_handlers(inst)
            elif type(inst) == types.TypeType:
                if inst.__name__.endswith("Handler"):
                    handlers.append((r"/%s/(.*)" % m.__name__.replace('.', '/'), inst))
                    handlers.append((r"/%s" % m.__name__.replace('.', '/'), inst))
                    print "loading handler", "/%s/(.*)" % m.__name__.replace('.', '/')
        return handlers

def main():
    port = util.config.get("global", "port")
    application = ESApplication( api, **{
        'debug': True
    })
    application.listen(port)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == '__main__':
    main()
