# coding=utf-8
#  -*- coding: <encoding name> -*-
import tornado.httpclient
from elasticsearch import Elasticsearch
from lxml.html.soupparser import fromstring
import random
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

class Opera:
    id = ''
    name = ''
    actor = ''
    type = ''
    link = ''
    def __init__(self, id, name, actor, type, link):
        self.id = id
        self.name = name
        self.actor = actor
        self.type = type
        self.link = link

class Spider:
    def __init__(self):
        es = Elasticsearch()
    def Fetch(self,url):
        user_agent_list = [
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 "
            "(KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1",
            "Mozilla/5.0 (X11; CrOS i686 2268.111.0) AppleWebKit/536.11 "
            "(KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 "
            "(KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6",
            "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.6 "
            "(KHTML, like Gecko) Chrome/20.0.1090.0 Safari/536.6",
            "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.1 "
            "(KHTML, like Gecko) Chrome/19.77.34.5 Safari/537.1",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/536.5 "
            "(KHTML, like Gecko) Chrome/19.0.1084.9 Safari/536.5",
            "Mozilla/5.0 (Windows NT 6.0) AppleWebKit/536.5 "
            "(KHTML, like Gecko) Chrome/19.0.1084.36 Safari/536.5",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 "
            "(KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
            "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.3 "
            "(KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_0) AppleWebKit/536.3 "
            "(KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 "
            "(KHTML, like Gecko) Chrome/19.0.1062.0 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 "
            "(KHTML, like Gecko) Chrome/19.0.1062.0 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 "
            "(KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 "
            "(KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/536.3 "
            "(KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 "
            "(KHTML, like Gecko) Chrome/19.0.1061.0 Safari/536.3",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.24 "
            "(KHTML, like Gecko) Chrome/19.0.1055.1 Safari/535.24",
            "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/535.24 "
            "(KHTML, like Gecko) Chrome/19.0.1055.1 Safari/535.24"
            ]
        headers = {'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Encoding':'gzip, deflate, sdch',
            'Accept-Language':'zh-CN,zh;q=0.8',
            'Cache-Control':'max-age=0',
            'Connection':'keep-alive',
            'Upgrade-Insecure-Requests':'1',
            'User-Agent':random.choice(user_agent_list)#'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'
        }
        http_request = tornado.httpclient.HTTPRequest(url=url,method='GET',headers=headers,connect_timeout=2000,request_timeout=6000)
        http_client = tornado.httpclient.HTTPClient()
        http_response = ''
        while 1:
            try:
                http_response = http_client.fetch(http_request)
                if isinstance(http_response, str):
                    pass
                content = str(http_response.body).decode('gbk')
                dom = fromstring(content)
                break
            except Exception as e:
                print e
                import time
                time.sleep(5)
                print http_response
        http_client.close()
        return content, dom

    def Get_Info(self, dom, url):
        id = 1
        for div in dom.iterdescendants('div'):
            if div.attrib.has_key('class') and div.attrib['class'] == 'content bord mtop':
                for li in div.iterdescendants('li'):
                    name = ''
                    actor = ''
                    type = ''
                    link = ''
                    atext = ''
                    now_url = url
                    for a in li.iterdescendants('a'):
                        now_url += a.attrib['href']
                        break
                    ocontent, odom = self.Fetch(now_url)
                    for div in odom.iterdescendants('div'):
                        if div.attrib.has_key('class') and div.attrib['class'] == 'g2':
                            for center in div.iterdescendants('center'):
                                name += center.text
                            for span in div.iterdescendants('span'):
                                if span.text:
                                    s = span.text.split('：')
                                    if s[0] == '戏曲说明':
                                        actor = s[1]
                                    if s[0] == '戏曲类型':
                                        type = s[1]
                        if div.attrib.has_key('class') and div.attrib['class'] == 'bord demand mtop':
                            flag = 0
                            for font in div.iterdescendants('font'):
                                if font.attrib.has_key('color') and font.attrib['color'] == 'red':
                                    if font.text == '下载 选择①号服务器 ':
                                        flag = 1
                                        break
                            if flag:
                                for divde in div.iterdescendants('div'):
                                    if divde.attrib.has_key('class') and divde.attrib['class'] == 'demand':
                                        for a in divde.iterdescendants('a'):
                                            link = a.attrib['href']
                                            atext = ' ' + a.text
                                            newOpera = Opera(id, name + atext, actor, type, link)
                                            es.index(index='program', doc_type='opera', body=newOpera.__dict__)
                                            id += 1
                    if link == '':
                        link = now_url + '/playbugu1.html'
                        newOpera = Opera(id, name + atext, actor, type, link)
                        es.index(index='program', doc_type='opera', body=newOpera.__dict__)

    def get_page(self, dom):
        for div in dom.iterdescendants('div'):
            if div.attrib.has_key('id') and div.attrib['id'] == 'page':
                for a in div.iterdescendants('a'):
                    if a.text == '尾页':
                        return a.attrib['href'].split('.')[0][5:]

    def get_list(self, dom):
        url_list = []
        for div in dom.iterdescendants('div'):
            if div.attrib.has_key('class') and div.attrib['class'] == 'acc2 ':
                for a in div.iterdescendants('a'):
                    url_list.append('http://www.xiqu5.com/' + a.attrib['href'].split('/')[1] + '/')
        return url_list

if __name__ == '__main__':
    # opera
    s = Spider()
    operac, operad = s.Fetch('http://www.xiqu5.com/')
    opera_list = s.get_list(operad)
    es = Elasticsearch()
    op_list = []
    for o in opera_list:
        oc,od = s.Fetch(o + 'index.html')
        s.Get_Info(od, o)
        page = s.get_page(od)
        for i in range(2, int(page)):
            oc,od = s.Fetch(o + 'index' + (str(i) if i > 1 else '') + '.html')
            s.Get_Info(od, o)
    pass

