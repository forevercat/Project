# coding=utf-8
#  -*- coding: <encoding name> -*-
import tornado.httpclient
import urllib2
import urllib
from elasticsearch import Elasticsearch
from lxml.html.soupparser import fromstring
import random
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
def getText(elem, singer):
    rc = []
    for node in elem.itertext():
        if node.strip()[0:2] == '演唱':
            if node.strip()[3:].replace(' ','') != singer.replace(' ',''):
                return '不是这个人'
        rc.append(node.strip())
    return ','.join(rc)
class Spider:
    def Fetch(self,url):
        user_agent_list = [
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 "
            "(KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1",
            "Mozilla/5.0 (X11; CrOS i686 2268.111.0) AppleWebKit/536.11 "
            "(KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 "
            "(KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6",
            "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.6 "
            "(KHTML, like Gecko) Chrome/20.0.1090.0 Safari/536.6",
            "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.1 "
            "(KHTML, like Gecko) Chrome/19.77.34.5 Safari/537.1",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/536.5 "
            "(KHTML, like Gecko) Chrome/19.0.1084.9 Safari/536.5",
            "Mozilla/5.0 (Windows NT 6.0) AppleWebKit/536.5 "
            "(KHTML, like Gecko) Chrome/19.0.1084.36 Safari/536.5",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 "
            "(KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
            "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.3 "
            "(KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_0) AppleWebKit/536.3 "
            "(KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 "
            "(KHTML, like Gecko) Chrome/19.0.1062.0 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 "
            "(KHTML, like Gecko) Chrome/19.0.1062.0 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 "
            "(KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 "
            "(KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/536.3 "
            "(KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 "
            "(KHTML, like Gecko) Chrome/19.0.1061.0 Safari/536.3",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.24 "
            "(KHTML, like Gecko) Chrome/19.0.1055.1 Safari/535.24",
            "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/535.24 "
            "(KHTML, like Gecko) Chrome/19.0.1055.1 Safari/535.24"
            ]
        headers = {'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Encoding':'gzip, deflate, sdch',
            'Accept-Language':'zh-CN,zh;q=0.8',
            'Cache-Control':'max-age=0',
            'Connection':'keep-alive',
            'Upgrade-Insecure-Requests':'1',
            'User-Agent':random.choice(user_agent_list)#'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'
        }
        http_request = tornado.httpclient.HTTPRequest(url=url,method='GET',headers=headers,connect_timeout=2000,request_timeout=6000)
        http_client = tornado.httpclient.HTTPClient()
        http_response = ''
        try:
            http_response = http_client.fetch(http_request)
        except Exception as e:
            print e
            print http_response
        finally:
            http_client.close()
        content = str(http_response.body)
        dom = fromstring(content)
        return content, dom

    def lyc(self, name, singer):
        c,d = self.Fetch('http://music.baidu.com/search/lrc?key=%s' % name)
        lyc = ''
        for p in d.iterdescendants('p'):
            if p.attrib.has_key('id') and p.attrib['id'][0:-1] == 'lyricCont-':
                lyc = getText(p, singer)
                if lyc != '不是这个人':
                    break
        return lyc.replace('\n',' ')

    def Get_Info(self,dom):
        name = ''
        singer = ''
        for span in dom.iterdescendants('span'):
            if span.tag == 'span' and span.attrib.has_key('class') and span.attrib['class'] == 'name':
                name = span.text
                break
        for div in dom.iterdescendants('div'):
            if div.attrib.has_key('class') and div.attrib['class'] == 'info-holder clearfix':
                for a in div.iterdescendants('a'):
                    if a.attrib.has_key('href') and a.attrib['href'][0:8] == '/artist/':
                        singer = a.text
                        break
        lyc = self.lyc(name, singer)
        return name, singer, lyc

    def get_music(self, dom):
        sing = []
        for span in dom.iterdescendants('span'):
            if span.tag == 'span' and span.attrib.has_key('class') and span.attrib['class'] == 'song-title ':
                for a in span.iterdescendants('a'):
                    sing.append({a.text:a.attrib['href']})
                    break
        return sing

class Music:
    id = ''
    sing = ''
    singer = ''
    lyrics = ''
    link = ''
    def __init__(self, id, sing, singer, lyrics, link):
        self.id = id
        self.sing = sing
        self.singer = singer
        self.lyrics = lyrics
        self.link = link

if __name__ == '__main__':
    # music
    s = Spider()
    mc,md = s.Fetch('http://music.baidu.com/top/dayhot/?pst=shouyeTop')
    ml = s.get_music(md)
    es = Elasticsearch()
    for m in ml:
        c,d = s.Fetch('http://music.baidu.com%s' % (m.values()[0]))
        name,singer,l = s.Get_Info(d)
        m = Music('1', name, singer, l,'http://music.baidu.com%s' % m.values()[0])
        es.index(index='program', doc_type='music', body=m.__dict__)
    pass

